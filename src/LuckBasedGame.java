import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class LuckBasedGame {

    private List<Player> players;
    private List<Player> tie_players= new ArrayList<>();
    private final int numOfPlayers;
    private final Deck deck;

    static final Logger log= LoggerFactory.getLogger(LuckBasedGame.class);

    public LuckBasedGame(int numOfPlayers, Deck deck) {
        this.numOfPlayers = numOfPlayers;
        this.deck = deck;
    }

    public boolean distributeCardsAmongPlayers() {
        if (validatePlayers()) {
            deck.shuffle();

            for(Player player: players)
                for (int i = 0; i < 3; i++){
                    Card card = deck.takeCard();
                    System.out.println("card num : " +i +" "+card);
                    player.getCardsDistributed().add(card);
                }


            players.forEach(player-> {
                log.info("{} : {}", player.getName(), player.getCardsDistributed());
            });
            return true;
        }
        return false;

    }


    public Boolean validatePlayers() {
        if (players.size() == numOfPlayers)
            return true;
        return false;
    }

    public Object show() {
        players.forEach(player->claculateValueOfCards(player));
        players.sort(Comparator.comparing(Player::getValueOfCards).reversed());
        if(players.get(0).getValueOfCards()!=players.get(1).getValueOfCards()) {
            log.info("Winner is {}", players.get(0).getName());
            return players.get(0);
        }
        else if(players.stream().mapToInt(Player::getValueOfCards).distinct().count()==1){
             tie_players=List.copyOf(players);
        }
        else if(players.stream().mapToInt(Player::getValueOfCards).distinct().count()==2 && players.get(2).getValueOfCards()==players.get(3).getValueOfCards()){
          //  if(players.get(2).getValueOfCards()==players.get(3).getValueOfCards())
                 tie_players=List.copyOf(players.subList(0,3));
           /* else
                 tie_players=List.copyOf(players.subList(0,1));*/
        }
        else
             tie_players=List.copyOf(players.subList(0,2));

        log.info("It's Tie Game for ");
        tie_players.forEach(player-> {
            log.info("{}", player.getName());
        });
        return tie_players;
    }

    private void claculateValueOfCards(Player player) {
        List<Card> playerCards=player.getCardsDistributed();
        playerCards.sort(Comparator.comparing(Card::getRank).reversed());
        if(playerCards.get(0)==playerCards.get(1) && playerCards.get(1)== playerCards.get(2))
            player.setValueOfCards(500+playerCards.stream().mapToInt(c->c.getRank().ordinal()).sum());
        else if(difference(playerCards.get(0),playerCards.get(1))==1 && difference(playerCards.get(1),playerCards.get(2))==1)
            player.setValueOfCards(400+playerCards.stream().mapToInt(c->c.getRank().ordinal()).sum());
        else if(difference(playerCards.get(0),playerCards.get(1))==0)
            player.setValueOfCards(300+playerCards.stream().mapToInt(c->c.getRank().ordinal()).sum());
        else
            player.setValueOfCards(200+playerCards.get(0).getRank().ordinal());
    }

    private int difference(Card card1, Card card2) {
        return Math.abs(card1.getRank().ordinal()-card2.getRank().ordinal());
    }


    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Player tie_show(List<Player> players) {
        deck.shuffle();
        players.forEach(player->{
            player.getCardsDistributed().clear();
            player.getCardsDistributed().add(deck.takeCard());
            log.info("{} card picked {}",player.getName(), player.getCardsDistributed());
            player.setValueOfCards(player.getCardsDistributed().get(0).getRank().ordinal());
        });
        players.sort(Comparator.comparing(Player::getValueOfCards).reversed());
        if(players.get(0).getValueOfCards()!=players.get(1).getValueOfCards()) {
            log.info("Winner is {}", players.get(0).getName());
            return players.get(0);
        }
        else if(players.stream().mapToInt(Player::getValueOfCards).distinct().count()==1) {
            log.info("Its a tie for ");
            players.forEach(player->log.info("{}",player.getName()));
            return tie_show(players);
        }
        else if(players.size()>3 && players.stream().mapToInt(Player::getValueOfCards).distinct().count()==2 && players.get(2).getValueOfCards()==players.get(3).getValueOfCards()){
            log.info("Its a tie for ");
            players.subList(0,3).forEach(player->log.info("{}",player.getName()));
            return tie_show(players.subList(0,3));
        }
        else {
            log.info("Its a tie for ");
            players.subList(0,2).forEach(player->log.info("{}",player.getName()));
            return tie_show(players.subList(0, 2));
        }

    }
}
