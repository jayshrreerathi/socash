public class Card {
    private final Rank rank;
    private final Suit suit;

    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public boolean isBigger(Card anotherCard) {
        if(this.getRank().ordinal()>anotherCard.getRank().ordinal())
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        return "Card{"+rank +" of " + suit +
                '}';
    }
}
