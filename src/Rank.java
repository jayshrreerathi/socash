public enum Rank {
    TWO,    //ordinal value: 0
    THREE,  //ordinal value: 1
    FOUR,   //ordinal value: 2
    FIVE,   //ordinal value: 3
    SIX,    //ordinal value: 4
    SEVEN,  //ordinal value: 5
    EIGHT,  //ordinal value: 6
    NINE,   //ordinal value: 7
    TEN,    //ordinal value: 8
    JACK,   //ordinal value: 9
    QUEEN,  //ordinal value: 10
    KING,   //ordinal value: 11
    ACE     //ordinal value: 12
}
