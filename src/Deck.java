import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Deck {
    public static final int SIZE =52 ;
    private List<Card> cards = new ArrayList<>();

    public Deck() {
        for (Suit suit: Suit.values())
            for (Rank rank: Rank.values())
                cards.add(new Card(rank, suit));
    }

    public int cardsRemaining() {
        return cards.size();
    }

    public boolean contains(Rank rank, Suit suit) {
        for (Card card: cards)
            if (rank == card.getRank() && suit == card.getSuit())
                return true;
        return false;

    }

    public Card takeCard() {
        if (cards.isEmpty())
            throw new IllegalStateException("Can't deal from an empty deck.");
        return cards.remove(0);
    }

    public List<Card> getCards() {
        return cards;
    }

    public void shuffle() {
        Collections.shuffle(cards);
    }
}
