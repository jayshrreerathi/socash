
import java.util.List;


public class Player {
    // not writing tests for getter setter
    private String name;
    private List<Card> cardsDistributed;

    public int getValueOfCards() {
        return valueOfCards;
    }

    public void setValueOfCards(int valueOfCards) {
        this.valueOfCards = valueOfCards;
    }

    private int valueOfCards;

    public Player(String name, List<Card> cardsDistributed) {
        this.name=name;
        this.cardsDistributed=cardsDistributed;
    }

    public List<Card> getCardsDistributed() {
        return cardsDistributed;
    }

    public String getName() {
        return name;
    }
}
