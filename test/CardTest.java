import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CardTest {

    @Test
    public void shouldCreateACard(){
        Card card=new Card(Rank.ACE,Suit.CLUBS);
        assertEquals(Rank.ACE, card.getRank());
        assertEquals(Suit.CLUBS, card.getSuit());
    }

    @Test
    public void shouldTest2AceEqualRank(){
        Card clubs_ace= new Card(Rank.ACE,Suit.CLUBS);
        Card diamonds_ace= new Card(Rank.ACE,Suit.DIAMONDS);
        assertEquals(clubs_ace.getRank(),diamonds_ace.getRank());
    }

    @Test
    public void shouldTestAceIsBiggerThanKing(){
        Card clubs_ace= new Card(Rank.ACE,Suit.CLUBS);
        Card diamonds_king= new Card(Rank.KING,Suit.DIAMONDS);
        assertTrue(clubs_ace.isBigger(diamonds_king));
        assertFalse(diamonds_king.isBigger(clubs_ace));
    }

}
