import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class LuckBasedGameTest {

    public Player p1= new Player("Player1",new ArrayList<Card>());
    public Player p2= new Player("Player2",new ArrayList<Card>());
    public Player p3= new Player("Player3",new ArrayList<Card>());
    public Player p4= new Player("Player4",new ArrayList<Card>());
    public List<Player> players=new ArrayList<>();
    public Deck deck=new Deck();
    public LuckBasedGame game= new LuckBasedGame(4,deck);

    @Test
    public void shouldEntertainExactly4Players(){
        //Arrange
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);

        game.setPlayers(players);

        //Act
        Boolean result = game.validatePlayers();

        //Assert
        assertTrue(result);
    }

    @Test
    public void shouldNotEntertainWhenNotExactly4Players(){
        //Arrange
        players.add(p1);
        players.add(p2);
        players.add(p3);

        game.setPlayers(players);
        //Act
        Boolean result = game.validatePlayers();

        //Assert
        assertFalse(result);
    }

    @Test
    public void shouldDistributeCardsAmongPlayers(){
        //Arrange
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);
        game.setPlayers(players);
        Deck deck1=mock(Deck.class);
        //Act
        when(deck1.takeCard()).thenCallRealMethod();
        boolean result = game.distributeCardsAmongPlayers();
        //Assert
        assertTrue(result);
    }

    @Test
    public void test_allPlayersShouldHaveExactly3CardsAfterDistribution(){
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);
        game.setPlayers(players);
        Deck deck1=mock(Deck.class);
        //Act
        when(deck1.takeCard()).thenCallRealMethod();
        boolean result = game.distributeCardsAmongPlayers();
        //Assert
        assertEquals(3,p1.getCardsDistributed().size());
        assertEquals(3,p2.getCardsDistributed().size());
        assertEquals(3,p3.getCardsDistributed().size());
        assertEquals(3,p4.getCardsDistributed().size());
    }

    @Test
    public void player1IsWinnerWithAllACE(){
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);
        game.setPlayers(players);
        //instead of calling method game.distributeCardsAmongPlayers(players) assigning each player distinct list of distinct cards
        p1.getCardsDistributed().add(new Card(Rank.ACE,Suit.CLUBS));
        p1.getCardsDistributed().add(new Card(Rank.ACE,Suit.DIAMONDS));
        p1.getCardsDistributed().add(new Card(Rank.ACE,Suit.HEARTS));


        p2.getCardsDistributed().add(new Card(Rank.FIVE,Suit.SPADES));
        p2.getCardsDistributed().add(new Card(Rank.TWO,Suit.SPADES));
        p2.getCardsDistributed().add(new Card(Rank.JACK,Suit.HEARTS));


        p3.getCardsDistributed().add(new Card(Rank.QUEEN,Suit.DIAMONDS));
        p3.getCardsDistributed().add(new Card(Rank.FOUR,Suit.SPADES));
        p3.getCardsDistributed().add(new Card(Rank.TEN,Suit.HEARTS));

        p4.getCardsDistributed().add(new Card(Rank.KING,Suit.DIAMONDS));
        p4.getCardsDistributed().add(new Card(Rank.QUEEN,Suit.SPADES));
        p4.getCardsDistributed().add(new Card(Rank.TWO,Suit.SPADES));

        Object result=game.show();
        //assertEquals("Winner is Player1",game.show());
        assertTrue(result instanceof Player);
        assertEquals(((Player)result).getName(),p1.getName());

    }

    @Test
    public void player1IsWinnerWithTwoACE() {
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);
        game.setPlayers(players);
        //instead of calling method game.distributeCardsAmongPlayers(players) assigning each player distinct list of distinct cards
        p1.getCardsDistributed().add(new Card(Rank.ACE, Suit.CLUBS));
        p1.getCardsDistributed().add(new Card(Rank.ACE, Suit.DIAMONDS));
        p1.getCardsDistributed().add(new Card(Rank.THREE, Suit.HEARTS));


        p2.getCardsDistributed().add(new Card(Rank.FIVE, Suit.SPADES));
        p2.getCardsDistributed().add(new Card(Rank.TWO, Suit.SPADES));
        p2.getCardsDistributed().add(new Card(Rank.JACK, Suit.HEARTS));


        p3.getCardsDistributed().add(new Card(Rank.QUEEN, Suit.DIAMONDS));
        p3.getCardsDistributed().add(new Card(Rank.FOUR, Suit.SPADES));
        p3.getCardsDistributed().add(new Card(Rank.TEN, Suit.HEARTS));

        p4.getCardsDistributed().add(new Card(Rank.KING, Suit.DIAMONDS));
        p4.getCardsDistributed().add(new Card(Rank.QUEEN, Suit.SPADES));
        p4.getCardsDistributed().add(new Card(Rank.TWO, Suit.SPADES));

        Object result = game.show();
        //assertEquals("Winner is Player1",game.show());
        assertTrue(result instanceof Player);
        assertEquals(((Player) result).getName(), p1.getName());
    }

    @Test
    public void player1IsTopCardWinner() {
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);
        game.setPlayers(players);
        //instead of calling method game.distributeCardsAmongPlayers(players) assigning each player distinct list of distinct cards
        p1.getCardsDistributed().add(new Card(Rank.FOUR, Suit.CLUBS));
        p1.getCardsDistributed().add(new Card(Rank.KING, Suit.DIAMONDS));
        p1.getCardsDistributed().add(new Card(Rank.THREE, Suit.HEARTS));


        p2.getCardsDistributed().add(new Card(Rank.FIVE, Suit.SPADES));
        p2.getCardsDistributed().add(new Card(Rank.TWO, Suit.SPADES));
        p2.getCardsDistributed().add(new Card(Rank.JACK, Suit.HEARTS));


        p3.getCardsDistributed().add(new Card(Rank.QUEEN, Suit.DIAMONDS));
        p3.getCardsDistributed().add(new Card(Rank.FOUR, Suit.SPADES));
        p3.getCardsDistributed().add(new Card(Rank.TEN, Suit.HEARTS));

        p4.getCardsDistributed().add(new Card(Rank.THREE, Suit.DIAMONDS));
        p4.getCardsDistributed().add(new Card(Rank.QUEEN, Suit.SPADES));
        p4.getCardsDistributed().add(new Card(Rank.TWO, Suit.SPADES));

        Object result = game.show();
        //assertEquals("Winner is Player1",game.show());
        assertTrue(result instanceof Player);
        assertEquals(((Player) result).getName(), p1.getName());
    }

    @Test
    public void itsAtieGame() {
        players.add(p1);
        players.add(p2);
        players.add(p3);
        players.add(p4);
        game.setPlayers(players);
        //instead of calling method game.distributeCardsAmongPlayers(players) assigning each player distinct list of distinct cards
        p1.getCardsDistributed().add(new Card(Rank.ACE, Suit.CLUBS));
        p1.getCardsDistributed().add(new Card(Rank.KING, Suit.DIAMONDS));
        p1.getCardsDistributed().add(new Card(Rank.NINE, Suit.HEARTS));


        p2.getCardsDistributed().add(new Card(Rank.FIVE, Suit.SPADES));
        p2.getCardsDistributed().add(new Card(Rank.TWO, Suit.SPADES));
        p2.getCardsDistributed().add(new Card(Rank.JACK, Suit.HEARTS));


        p3.getCardsDistributed().add(new Card(Rank.QUEEN, Suit.DIAMONDS));
        p3.getCardsDistributed().add(new Card(Rank.FOUR, Suit.SPADES));
        p3.getCardsDistributed().add(new Card(Rank.TEN, Suit.HEARTS));

        p4.getCardsDistributed().add(new Card(Rank.NINE, Suit.DIAMONDS));
        p4.getCardsDistributed().add(new Card(Rank.KING, Suit.SPADES));
        p4.getCardsDistributed().add(new Card(Rank.ACE, Suit.SPADES));

        Object result = game.show();
        //assertEquals("Winner is Player1",game.show());
        assertTrue(result instanceof List);
        assertEquals(2,((List<Player>) result).size());
    }

    @Test
    public void aGameForTiePlayers() {
        players.add(p1);
        players.add(p2);
        players.add(p3);

       assertNotNull(game.tie_show(players));


    }
}
