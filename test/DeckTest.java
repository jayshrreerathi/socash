import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DeckTest {

    @Test
    public void shouldCreateDeckOf52Cards(){
        Deck deck = new Deck();
        assertEquals(Deck.SIZE,deck.cardsRemaining());
    }


    @Test
    public void shouldContain_a_cardForEachRankAndSuit() {
        Deck deck = new Deck();
        assertEquals(Deck.SIZE,deck.cardsRemaining());
        for (Suit suit: Suit.values())
            for (Rank rank: Rank.values())
                assertTrue(deck.contains(rank, suit));
    }

    @Test
    public void shouldTakeCardFromDeck(){
        Deck deck = new Deck();
        assertNotNull(deck.takeCard());
        assertEquals(51,deck.cardsRemaining());
    }

    @Test
    public void shouldThrowExceptionWhenNoCardLeftInDeck(){
        Deck deck = new Deck();
        for(int i=0;i<52;i++)
            deck.takeCard();
        assertThrows(IllegalStateException.class, ()-> deck.takeCard());
    }

    @Test
    public void shouldReturnSameSizeWhenShuffled(){
       Deck deck= new Deck();
       deck.shuffle();
       assertEquals(Deck.SIZE,deck.cardsRemaining());
    }

}
